module.exports =
  containers:
    REGISTRY: process.env.CI_REGISTRY || 'registry.gitlab.com'
    IMAGE: process.env.CI_PROJECT_PATH || 'ephracis/foreman'
    REGISTRY_USER: process.env.CI_REGISTRY_USER || process.env.REGISTRY_USER ||
                   '$(whoami)'
    REGISTRY_PASS: process.env.CI_REGISTRY_PASSWORD || process.env.REGISTRY_PASS
