all_names = ['foreman']
image = (name) -> "$REGISTRY/$IMAGE/#{name}"

docker_build = (name) ->
  """
  docker build -t $REGISTRY/$IMAGE/#{name} \
               --build-arg version=<%= githash.main.tag %> \
               #{name}
  docker tag $REGISTRY/$IMAGE/#{name} \
             $REGISTRY/$IMAGE/#{name}:<%= githash.main.tag %>
  """

docker_save = (name) ->
  """
  docker save $REGISTRY/$IMAGE/#{name}:latest > \
         ansible/roles/fixtures/files/#{name}.tar
  """

docker_push = (name) ->
  """
  docker login -u $REGISTRY_USER -p $REGISTRY_PASS $REGISTRY
  docker push $REGISTRY/$IMAGE/#{name}:latest
  docker push $REGISTRY/$IMAGE/#{name}:<%= githash.main.tag %>
  """

docker_lint = (name) ->
  """
  dockerfile_lint -f #{name}/Dockerfile
  """

docker_test = (name) ->
  """
  docker build -t $REGISTRY/$IMAGE/#{name}:test test/#{name}
  docker run --rm $REGISTRY/$IMAGE/#{name}:test
  """

docker_clean = (name) ->
  """
  docker rmi $REGISTRY/$IMAGE/#{name}:test
  """

module.exports =
  docker:
    command: (action, name) ->
      names = if name == 'undefined' then all_names else [name]
      cmds = names.map (n) ->
        switch action
          when 'build' then docker_build(n)
          when 'save' then docker_save(n)
          when 'test' then docker_test(n)
          when 'push' then docker_push(n)
          when 'lint' then docker_lint(n)
          when 'clean' then docker_clean(n)
      cmds.join "\n"
