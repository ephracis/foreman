describe http('http://localhost:3000') do
  its(:status) { should eq 302 }
end

describe http('http://localhost:3000/users/login') do
  its(:status) { should eq 200 }
  its(:body) { should match 'Welcome to Foreman' }
  its(:body) { should match 'Log In' }
end
